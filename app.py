from flask import Flask, render_template, request, flash
from flask import make_response
from flask_sqlalchemy import SQLAlchemy
from geopy.geocoders import Nominatim
from geopy.exc import GeocoderTimedOut
from sqlalchemy import func
import gmplot
import ast
import pygal

app=Flask(__name__)
#app.config['SQLALCHEMY_DATABASE_URI']='postgres://postgres:Krsn@_Das108@localhost:5432/cannabis_opinion_survey'
app.config['SQLALCHEMY_DATABASE_URI']='postgres://pazkxxqvjonyjl:c5931f15ca640860eae05974952b01724b67cddbbaca49549c9eca64739fbb27@ec2-50-19-83-146.compute-1.amazonaws.com:5432/db1lma346on8js?sslmode=require'
db=SQLAlchemy(app)

class Cannabis(db.Model):
    __tablename__="data"
    id=db.Column(db.Integer, primary_key=True)
    legalization=db.Column(db.String(4))
    consume=db.Column(db.String(3))
    mode=db.Column(db.String(20))
    strain=db.Column(db.String(6))
    age=db.Column(db.String(5))
    city=db.Column(db.String(50))
    state=db.Column(db.String(50))
    latitude=db.Column(db.Numeric(precision=20, asdecimal=True, decimal_return_scale=7))
    longitude=db.Column(db.Numeric(precision=20, asdecimal=True, decimal_return_scale=7))
    ip=db.Column(db.String(50), unique=False)

    def __init__(self, legalization, consume, mode, strain, age, city, state, latitude, longitude, ip):
        self.legalization=legalization
        self.consume=consume
        self.mode=mode
        self.strain=strain
        self.age=age
        self.city=city
        self.state=state
        self.latitude=latitude
        self.longitude=longitude
        self.ip=ip

class Results(object):
    # Total, legalization of some form, against, medical only totals
    total_surveyed=db.session.query(func.count(Cannabis.id)).scalar()
    for_legalization=Cannabis.query.filter_by(legalization="Full").count()
    against_legalization=Cannabis.query.filter_by(legalization="No").count()
    medical_legalization=Cannabis.query.filter_by(legalization="Med").count()

    # 17 and younger demographic
    seventeen_younger_consume=Cannabis.query.filter_by(age="17", consume="Yes").count()
    seventeen_younger_non_consume=Cannabis.query.filter_by(age="17", consume="No").count()
    seventeen_younger_for=Cannabis.query.filter_by(age="17", legalization="Full").count()
    seventeen_younger_against=Cannabis.query.filter_by(age="17", legalization="No").count()
    seventeen_younger_medical=Cannabis.query.filter_by(age="17", legalization="Med").count()

    # 18-24 demographic
    eighteen_twentyfour_consume=Cannabis.query.filter_by(age="18-24", consume="Yes").count()
    eighteen_twentyfour_non_consume=Cannabis.query.filter_by(age="18-24", consume="No").count()
    eighteen_twentyfour_for=Cannabis.query.filter_by(age="18-24", legalization="Full").count()
    eighteen_twentyfour_against=Cannabis.query.filter_by(age="18-24", legalization="No").count()
    eighteen_twentyfour_medical=Cannabis.query.filter_by(age="18-24", legalization="Med").count()

    # 25-34 demographic
    twentyfive_thirtyfour_consume=Cannabis.query.filter_by(age="25-34", consume="Yes").count()
    twentyfive_thirtyfour_non_consume=Cannabis.query.filter_by(age="25-34", consume="No").count()
    twentyfive_thirtyfour_for=Cannabis.query.filter_by(age="25-34", legalization="Full").count()
    twentyfive_thirtyfour_against=Cannabis.query.filter_by(age="25-34", legalization="No").count()
    twentyfive_thirtyfour_medical=Cannabis.query.filter_by(age="25-34", legalization="Med").count()

    # 35-44 demographic
    thirtyfive_fourtyfour_consume=Cannabis.query.filter_by(age="35-44", consume="Yes").count()
    thirtyfive_fourtyfour_non_consume=Cannabis.query.filter_by(age="35-44", consume="No").count()
    thirtyfive_fourtyfour_for=Cannabis.query.filter_by(age="35-44", legalization="Full").count()
    thirtyfive_fourtyfour_against=Cannabis.query.filter_by(age="35-44", legalization="No").count()
    thirtyfive_fourtyfour_medical=Cannabis.query.filter_by(age="35-44", legalization="Med").count()

    # 45-54 demographic
    fourtyfive_fiftyfour_consume=Cannabis.query.filter_by(age="45-54", consume="Yes").count()
    fourtyfive_fiftyfour_non_consume=Cannabis.query.filter_by(age="45-54", consume="No").count()
    fourtyfive_fiftyfour_for=Cannabis.query.filter_by(age="45-54", legalization="Full").count()
    fourtyfive_fiftyfour_against=Cannabis.query.filter_by(age="45-54", legalization="No").count()
    fourtyfive_fiftyfour_medical=Cannabis.query.filter_by(age="45-54", legalization="Med").count()

    # 55-64 demographic
    fiftyfive_sixtyfour_consume=Cannabis.query.filter_by(age="55-64", consume="Yes").count()
    fiftyfive_sixtyfour_non_consume=Cannabis.query.filter_by(age="55-64", consume="No").count()
    fiftyfive_sixtyfour_for=Cannabis.query.filter_by(age="55-64", legalization="Full").count()
    fiftyfive_sixtyfour_against=Cannabis.query.filter_by(age="55-64", legalization="No").count()
    fiftyfive_sixtyfour_medical=Cannabis.query.filter_by(age="55-64", legalization="Med").count()

    # 65 and older demographic
    sixtyfive_older_consume=Cannabis.query.filter_by(age="65", consume="Yes").count()
    sixtyfive_older_non_consume=Cannabis.query.filter_by(age="65", consume="No").count()
    sixtyfive_older_for=Cannabis.query.filter_by(age="65", legalization="Full").count()
    sixtyfive_older_against=Cannabis.query.filter_by(age="65", legalization="No").count()
    sixtyfive_older_medical=Cannabis.query.filter_by(age="65", legalization="Med").count()

    # Non-consumer for legalization & consumer against full legalization
    non_consumer_medical = Cannabis.query.filter_by(consume="No", legalization="Med").count()
    non_consumer_full = Cannabis.query.filter_by(consume="No", legalization="Full").count()
    non_consumer_against = Cannabis.query.filter_by(consume="No", legalization="No").count()
    consumer_medical_only = Cannabis.query.filter_by(consume="Yes", legalization="Med").count()
    consumer_against_legal = Cannabis.query.filter_by(consume="Yes", legalization="No").count()
    consumer_full = Cannabis.query.filter_by(consume="Yes", legalization="Full").count()

    # Consume = Yes and strain preferences
    total_consumer = Cannabis.query.filter_by(consume="Yes").count()
    consumer_sativa = Cannabis.query.filter_by(consume="Yes", strain="Sativa").count()
    consumer_indica = Cannabis.query.filter_by(consume="Yes", strain="Indica").count()
    consumer_hybrid = Cannabis.query.filter_by(consume="Yes", strain="Hybrid").count()
    # Consume = Yes and favored method of consumption
    consumer_smoke = Cannabis.query.filter_by(consume="Yes", mode="Smoke").count()
    consumer_vape = Cannabis.query.filter_by(consume="Yes", mode="Vape").count()
    consumer_edible = Cannabis.query.filter_by(consume="Yes", mode="Edible").count()
    consumer_dabbing = Cannabis.query.filter_by(consume="Yes", mode="Dabbing").count()
    consumer_topical = Cannabis.query.filter_by(consume="Yes", mode="Topical").count()
    consumer_ingestible_oil = Cannabis.query.filter_by(consume="Yes", mode="Ingestible Oil").count()
    consumer_tincture = Cannabis.query.filter_by(consume="Yes", mode="Tincture").count()
    # Consume = No
    total_non_consumer = Cannabis.query.filter_by(consume="No").count()


    # Legalization stats by percentage
    try:
        perc_int_for_legalization= round(100 * for_legalization / total_surveyed, 2)
        perc_int_against_legalization= round(100 * against_legalization / total_surveyed, 2)
        perc_int_medical_legalization= round(100 * medical_legalization / total_surveyed, 2)
        # legalization percentage on age demographic
        # 17 and younger
        perc_int_17_legalization = round(100 * seventeen_younger_for / total_surveyed, 2)
        perc_int_17_against_lagalization = round(100 * seventeen_younger_against / total_surveyed, 2)
        perc_int_17_medical_legalization = round(100 * seventeen_younger_medical / total_surveyed, 2)
        # 18-24
        perc_int_18_24_legalization = round(100 * eighteen_twentyfour_for / total_surveyed, 2)
        perc_int_18_24_against_legalization = round(100 * eighteen_twentyfour_against / total_surveyed, 2)
        perc_int_18_24_medical_legalization = round(100 * eighteen_twentyfour_medical / total_surveyed, 2)
        # 25-34
        perc_int_25_34_legalization = round(100 * twentyfive_thirtyfour_for / total_surveyed, 2)
        perc_int_25_34_against_legalization = round(100 * twentyfive_thirtyfour_against / total_surveyed, 2)
        perc_int_25_34_medical_legalization = round(100 * twentyfive_thirtyfour_medical / total_surveyed, 2)
        # 35-44
        perc_int_35_44_legalization = round(100 * thirtyfive_fourtyfour_for / total_surveyed, 2)
        perc_int_35_44_against_legalization = round(100 * thirtyfive_fourtyfour_against / total_surveyed, 2)
        perc_int_35_44_medical_legalization = round(100 * thirtyfive_fourtyfour_medical / total_surveyed, 2)
        # 45-54
        perc_int_45_54_legalization = round(100 * fourtyfive_fiftyfour_for / total_surveyed, 2)
        perc_int_45_54_against_legalization = round(100 * fourtyfive_fiftyfour_against / total_surveyed, 2)
        perc_int_45_54_medical_legalization = round(100 * fourtyfive_fiftyfour_medical / total_surveyed, 2)
        # 55-64
        perc_int_55_64_legalization = round(100 * fiftyfive_sixtyfour_for / total_surveyed, 2)
        perc_int_55_64_against_legalization = round(100 * fiftyfive_sixtyfour_against / total_surveyed, 2)
        perc_int_55_64_medical_legalization = round(100 * fiftyfive_sixtyfour_medical / total_surveyed, 2)
        #65 and older
        perc_int_65_legalization = round(100 * sixtyfive_older_for / total_surveyed, 2)
        perc_int_65_against_lagalization = round(100 * sixtyfive_older_against / total_surveyed, 2)
        perc_int_65_medical_legalization = round(100 * sixtyfive_older_medical / total_surveyed, 2)
        # Consumer percentage statistics
        perc_consumer_medical = round(100 * consumer_medical_only / total_surveyed, 2)
        perc_consumer_against = round(100 * consumer_against_legal / total_surveyed, 2)
        perc_consumer_full = round(100 * consumer_full / total_surveyed, 2)
        # Non-consumer percentage statistics
        perc_non_consumer_medical = round(100 * non_consumer_medical / total_surveyed, 2)
        perc_non_consumer_full = round(100 * non_consumer_full / total_surveyed, 2)
        perc_non_consumer_against = round(100 * non_consumer_against / total_surveyed, 2)
    except ZeroDivisionError:
        perc_int_for_legalization= 0
        perc_int_against_legalization= 0
        perc_int_medical_legalization= 0
        # legalization percentage on age demographic
        # 17 and younger
        perc_int_17_legalization = 0
        perc_int_17_against_lagalization = 0
        perc_int_17_medical_legalization = 0
        # 18-24
        perc_int_18_24_legalization = 0
        perc_int_18_24_against_legalization = 0
        perc_int_18_24_medical_legalization = 0
        # 25-34
        perc_int_25_34_legalization = 0
        perc_int_25_34_against_legalization = 0
        perc_int_25_34_medical_legalization = 0
        # 35-44
        perc_int_35_44_legalization = 0
        perc_int_35_44_against_legalization = 0
        perc_int_35_44_medical_legalization = 0
        # 45-54
        perc_int_45_54_legalization = 0
        perc_int_45_54_against_legalization = 0
        perc_int_45_54_medical_legalization = 0
        # 55-64
        perc_int_55_64_legalization = 0
        perc_int_55_64_against_legalization = 0
        perc_int_55_64_medical_legalization = 0
        #65 and older
        perc_65_legalization=0
        perc_int_65_legalization = 0
        perc_65_against_lagalization=0
        perc_int_65_against_lagalization = 0
        perc_65_medical_legalization=0
        perc_int_65_medical_legalization = 0
        # Consumer percentage statistics
        perc_consumer_medical = 0
        perc_consumer_against = 0
        perc_consumer_full = 0
        # Non-consumer percentage statistics
        perc_non_consumer_medical = 0
        perc_non_consumer_full = 0
        perc_non_consumer_against = 0

    # Consumer stats by percentage
    try:
        # Consumer and strain
        perc_int_sativa = round(100 * consumer_sativa / total_consumer, 2)
        perc_int_indica = round(100 * consumer_indica / total_consumer, 2)
        perc_int_hybrid = round(100 * consumer_hybrid / total_consumer, 2)
        # Consumer age demographic
        perc_int_17_consume = round(100 * seventeen_younger_consume / total_consumer, 2)
        perc_int_18_24_consume = round(100 * eighteen_twentyfour_consume / total_consumer, 2)
        perc_int_25_34_consume = round(100 * twentyfive_thirtyfour_consume / total_consumer, 2)
        perc_int_35_44_consume = round(100 * thirtyfive_fourtyfour_consume / total_consumer, 2)
        perc_int_45_54_consume = round(100 * fourtyfive_fiftyfour_consume / total_consumer, 2)
        perc_int_55_64_consume = round(100 * fiftyfive_sixtyfour_consume / total_consumer, 2)
        perc_int_65_consume = round(100 * sixtyfive_older_consume / total_consumer, 2)
        # Non-consumer age demographic
        perc_int_17_non_consume = round(100 * seventeen_younger_non_consume / total_non_consumer, 2)
        perc_int_18_24_non_consume = round(100 * eighteen_twentyfour_non_consume / total_non_consumer, 2)
        perc_int_25_34_non_consume = round(100 * twentyfive_thirtyfour_non_consume / total_non_consumer, 2)
        perc_int_35_44_non_consume = round(100 * thirtyfive_fourtyfour_non_consume / total_non_consumer, 2)
        perc_int_45_54_non_consume = round(100 * fourtyfive_fiftyfour_non_consume / total_non_consumer, 2)
        perc_int_55_64_non_consume = round(100 * fiftyfive_sixtyfour_non_consume / total_non_consumer, 2)
        perc_int_65_non_consume = round(100 * sixtyfive_older_non_consume / total_non_consumer, 2)
        # Consumer and method of consumption
        perc_int_smoke = round(100 * consumer_smoke / total_consumer, 2)
        perc_int_vape = round(100 * consumer_vape / total_consumer, 2)
        perc_int_edible = round(100 * consumer_edible / total_consumer, 2)
        perc_int_dabbing = round(100 * consumer_dabbing / total_consumer, 2)
        perc_int_topical = round(100 * consumer_topical / total_consumer, 2)
        perc_int_tincture = round(100 * consumer_tincture / total_consumer, 2)
        perc_int_ingestible_oil = round(100 * consumer_ingestible_oil / total_consumer, 2)
    except ZeroDivisionError:
        # Consumer and strain
        perc_int_sativa = 0
        perc_int_indica = 0
        perc_int_hybrid = 0
        # Consumer age demographic
        perc_int_17_consume = 0
        perc_int_18_24_consume = 0
        perc_int_25_34_consume = 0
        perc_int_35_44_consume = 0
        perc_int_45_54_consume = 0
        perc_int_55_64_consume = 0
        perc_int_65_consume = 0
        # Non-consumer age demographic
        perc_int_17_non_consume = 0
        perc_int_18_24_non_consume = 0
        perc_int_25_34_non_consume = 0
        perc_int_35_44_non_consume = 0
        perc_int_45_54_non_consume = 0
        perc_int_55_64_non_consume = 0
        perc_int_65_non_consume = 0
        # Consumer and method of consumption
        perc_int_smoke = 0
        perc_int_vape = 0
        perc_int_edible = 0
        perc_int_dabbing = 0
        perc_int_topical = 0
        perc_int_tincture = 0
        perc_int_ingestible_oil = 0

# or set directly on the app
app.secret_key = '!8gWjyw!hPxOB7(FH^WY0*B^_Cfmb^aOLO'

@app.route('/')
def index():
    return render_template("index.html")

@app.route("/results", methods=['POST', 'GET'])
def results():
    nom=Nominatim(country_bias='usa',scheme='http')
    # Gathering variable values to insert into DB
    if request.method=='POST':
        voted = "I voted!"
        legalization=request.form["legalization"]
        consume=request.form["consume"]
        if consume=='Yes':
            mode=request.form["mode"]
            strain=request.form["strain"]
        else:
            mode='Null'
            strain='Null'
        age=request.form["age"]
        city=request.form["city"]
        city= city.title()
        state=request.form["state"]
        guest_ip=request.environ['REMOTE_ADDR']
        place={"city":city, "state":state, "country": "USA"}
        # Check if City and State match
        city_correct=city_in_state(city, state, place)
        # Cookie check to see if user voted:
        if 'voted' in request.cookies:
            legalization_chart(Results)
            age_demo_chart(Results)
            consumer_vs_non_chart(Results)
            consumer_strain_chart(Results)
            consumer_age_chart(Results)
            consumer_mode_chart(Results)
            return render_template('results.html', result=Results)
        else:
            # As long as City exists in State
            if city_correct=="true":
                address=nom.geocode(place)
                data=Cannabis(legalization, consume, mode, strain, age, city, state, address.latitude, address.longitude, guest_ip)
                db.session.add(data)
                db.session.commit()
                legalization_chart(Results)
                age_demo_chart(Results)
                consumer_vs_non_chart(Results)
                consumer_strain_chart(Results)
                consumer_age_chart(Results)
                consumer_mode_chart(Results)
                # Set cookies
                resp = make_response(render_template('results.html'))
                resp.set_cookie('voted', voted)
                return resp
            else:
                flash('City does not exist in State')
                return render_template("index.html")
# Validates whether the state in the address is the same as the users chosen state
def city_in_state(city, state, place):
    nom=Nominatim(country_bias='usa',scheme='http')
    try:
        address=nom.geocode(place)
        if address != None:
            if state in address.address and city in address.address:
                return "true"
            else:
                return "false"
    except GeocoderTimedOut:
        flash('Issue with geocoding, try again!')
        return render_template("index.html")


def legalization_chart(Results):
    pie_chart = pygal.Pie()
    pie_chart.title = 'Cannabis Legalization in USA (in %)'
    pie_chart.add('Full', Results.perc_int_for_legalization)
    pie_chart.add('Medical', Results.perc_int_medical_legalization)
    pie_chart.add('Against', Results.perc_int_against_legalization)
    pie_chart.render_to_file('templates/legalization_chart.svg')
    move_charts_over('legalization_chart.svg')

def age_demo_chart(Results):
    line_chart = pygal.Bar()
    line_chart.title = "Age demographics and Cannabis legalization in USA (in %)"
    line_chart.x_labels = map(str, ["17 & under", "18-24", "25-34", "35-44", "45-54", "55-64", "65 & older"])
    line_chart.add('Full', [Results.perc_int_17_legalization, Results.perc_int_18_24_legalization, Results.perc_int_25_34_legalization, Results.perc_int_35_44_legalization, Results.perc_int_45_54_legalization, Results.perc_int_55_64_legalization, Results.perc_int_65_legalization])
    line_chart.add('Medical', [Results.perc_int_17_medical_legalization, Results.perc_int_18_24_medical_legalization, Results.perc_int_25_34_medical_legalization, Results.perc_int_35_44_medical_legalization, Results.perc_int_45_54_medical_legalization, Results.perc_int_55_64_medical_legalization, Results.perc_int_65_medical_legalization])
    line_chart.add('Against', [Results.perc_int_17_against_lagalization, Results.perc_int_18_24_against_legalization, Results.perc_int_25_34_against_legalization, Results.perc_int_35_44_against_legalization, Results.perc_int_45_54_against_legalization, Results.perc_int_55_64_against_legalization, Results.perc_int_65_against_lagalization])
    line_chart.render_to_file('templates/age_demo_chart.svg')
    move_charts_over('age_demo_chart.svg')

def consumer_age_chart(Results):
    line_chart = pygal.Bar()
    line_chart.title = "Cannabis consumer and non-consumer age demographics in USA (in %)"
    line_chart.x_labels = map(str, ["17 & under", "18-24", "25-34", "35-44", "45-54", "55-64", "65 & older"])
    line_chart.add('Consumers', [Results.perc_int_17_consume, Results.perc_int_18_24_consume, Results.perc_int_25_34_consume, Results.perc_int_35_44_consume, Results.perc_int_45_54_consume, Results.perc_int_55_64_consume, Results.perc_int_65_consume])
    line_chart.add('Non-consumers', [Results.perc_int_17_non_consume, Results.perc_int_18_24_non_consume, Results.perc_int_25_34_non_consume, Results.perc_int_35_44_non_consume, Results.perc_int_45_54_non_consume, Results.perc_int_55_64_non_consume, Results.perc_int_65_non_consume])
    line_chart.render_to_file('templates/consumer_age_chart.svg')
    move_charts_over('consumer_age_chart.svg')

def consumer_vs_non_chart(Results):
    cvn_chart = pygal.Bar()
    cvn_chart.title = 'Consumers and non-consumers of Cannabis and legalization in USA (in %)'
    cvn_chart.x_labels = map(str, ["Consumers", "Non-consumers"])
    cvn_chart.add('Full', [Results.perc_consumer_full, Results.perc_non_consumer_full])
    cvn_chart.add('Medical', [Results.perc_consumer_medical, Results.perc_non_consumer_medical])
    cvn_chart.add('Against', [Results.perc_consumer_against, Results.perc_non_consumer_against])
    cvn_chart.render_to_file('templates/consumer_vs_non_chart.svg')
    move_charts_over('consumer_vs_non_chart.svg')

def consumer_strain_chart(Results):
    pie_chart = pygal.Pie()
    pie_chart.title = 'Cannabis strain in USA (in %)'
    pie_chart.add('Sativa', Results.perc_int_sativa)
    pie_chart.add('Indica', Results.perc_int_indica)
    pie_chart.add('Hybrid', Results.perc_int_hybrid)
    pie_chart.render_to_file('templates/consumer_strain_chart.svg')
    move_charts_over('consumer_strain_chart.svg')

def consumer_mode_chart(Results):
    pie_chart = pygal.Pie()
    pie_chart.title =  'Method of consuming Cannabis in USA (in %)'
    pie_chart.add('Smoke', Results.perc_int_smoke)
    pie_chart.add('Vape', Results.perc_int_vape)
    pie_chart.add('Edible', Results.perc_int_edible)
    pie_chart.add('Dabbing', Results.perc_int_dabbing)
    pie_chart.add('Ingestible Oil', Results.perc_int_ingestible_oil)
    pie_chart.add('Topical', Results.perc_int_topical)
    pie_chart.add('Tincture', Results.perc_int_tincture)
    pie_chart.render_to_file('templates/consumer_mode_chart.svg')
    move_charts_over('consumer_mode_chart.svg')

def move_charts_over(filename):
    import os, shutil
    filename = filename
    src_dir = 'templates/'
    dst_dir = 'static/'
    src_file = os.path.join(src_dir, filename)
    dst_file = os.path.join(dst_dir, filename)
    if os.path.exists(dst_file):
        os.remove(dst_file)
    shutil.move(src_file, dst_dir)


if __name__=="__main__":
    app.debug=True
    app.run()
